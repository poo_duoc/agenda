/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

import java.util.ArrayList;

/**
 *
 * @author Duoc
 */
public class Contactos {
    
    private ArrayList<Persona> personas;

    public Contactos() {
        personas = new ArrayList<Persona>();
    }
    
    public void agregarPersona(Persona newPersona){
        boolean existeNumero = false;
        for(Persona aux : personas){
            if(aux.getNumeroCelular().equals(newPersona.getNumeroCelular())){
              existeNumero = true;
              break;
            }
        }
        if(existeNumero == true){
            System.out.println("Numero ya existe");
        }else{
            personas.add(newPersona);
        }
        
    }
    
    public int obtenerTotalPersonas(String letra){
        int contador = 0;
        for(Persona aux : personas){
            if(aux.getNombre().substring(0, 1).toLowerCase().equals(letra.toLowerCase())){
                contador++;
            }
        }
        return contador;
    }
    
    
    
}
