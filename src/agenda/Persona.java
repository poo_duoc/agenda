/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

/**
 *
 * @author Duoc
 */
public class Persona {
    private String nombre;
    private String numeroCelular;
    private String direccion;
    private String email;

    public Persona(String nombre, String numeroCelular, String direccion, String email) {
        this.nombre = nombre;
        this.numeroCelular = numeroCelular;
        this.direccion = direccion;
        this.email = email;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumeroCelular() {
        return numeroCelular;
    }

    public void setNumeroCelular(String numeroCelular) {
        this.numeroCelular = numeroCelular;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public void imprimir(){
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "Nombre: " + getNombre() + 
                ", Email: " + getEmail() + ", " + 
                "Direccion: " + getDireccion() + 
                ", Numero Celular: " + getNumeroCelular(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
